@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{route('sales.create')}}" class="btn btn-success">Add sale</a>
                    <a href="{{route('sales.index')}}" class="btn btn-info">View sales record</a>
                    <a href="{{route('reports')}}" class="btn btn-warning">View Reports</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
