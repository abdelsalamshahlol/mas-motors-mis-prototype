@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="panel-body">
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
            @endif
        <div class="">
            <div class="panel panel-info">
                <div class="panel-heading">Reports</div>
                  <div class="panel-body">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-6">
                          <h3>Total Number of units sold</h3>
                        </div>
                        <div class="col-md-6">
                            <h3>{{$sales_count}} Unit</h3>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <h3>Models</h3>
                        </div>
                        <div class="col-md-6">
                            @foreach ($models as $d)
                              <h3>{{"-".$d}}</h3>
                            @endforeach
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-8 ">
                          <h3>Number of units sold - Grouped by model</h3>
                          <canvas id="bar-chart" width="" height="80"></canvas>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-8">
                          <h3>Vehicle sales income per year</h3>
                          <canvas id="pie-chart" width="" height="180"></canvas>
                        </div>
                      </div>
                    </div>
                      <br>
                      <hr>
                      <a href="{{url()->route('home')}}" class="btn btn-warning">Go back</a>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: [ @foreach($models as $model)"{{$model}}",@endforeach],
      datasets: [
        {      label: "Sales per model",
          backgroundColor: ["#3e95cd", "#8e5ea2","#c45850"],
          data: [ @foreach($perModel as $model_data){{count($model_data)}},@endforeach 0],
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
      }
    }
  });
  //end of first Chart

  new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: {
      labels: [@foreach ($sales_yearly as $data)
        {{$data[0]}},
      @endforeach],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data: [@foreach ($sales_yearly as $data)
          {{$data[1]}},
        @endforeach,]
      }]
    },
    options: {
      title: {
        display: true,
      }
    }
});
  //end of second chart
});


</script>
@endsection
