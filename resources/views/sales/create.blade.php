@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add to sales record</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" action="{{route('sales.store')}}" method="post">
                      <fieldset>
                        {{csrf_field()}}

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Name</label>
                        <div class="col-md-4">
                        <input id="name" name="name" type="text" placeholder="" class="form-control input-md">

                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="address">Address</label>
                        <div class="col-md-6">
                        <input id="address" name="address" type="text" placeholder="" class="form-control input-md">

                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="Phone">Phone</label>
                        <div class="col-md-4">
                        <input id="Phone" name="Phone" type="text" placeholder="" class="form-control input-md">

                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Email</label>
                        <div class="col-md-4">
                        <input id="email" name="email" type="text" placeholder="" class="form-control input-md">

                        </div>
                      </div>

                      <!-- Select Basic -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="model">Model</label>
                        <div class="col-md-4">
                          <select id="model" name="model" class="form-control">
                            <option value="LC200 GXR">LC200 GXR</option>
                            <option value="NG Corolla">NG Corolla</option>
                            <option value="Hilux DC 2.0">Hilux DC 2.0</option>
                          </select>
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="color">Color</label>
                        <div class="col-md-4">
                        <input id="color" name="color" type="text" placeholder="" class="form-control input-md">

                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="vin">VIN #</label>
                        <div class="col-md-4">
                        <input id="vin" name="vin" type="text" placeholder="" class="form-control input-md">

                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="sfx">SFX</label>
                        <div class="col-md-4">
                          <label class="radio-inline" for="sfx-0">
                            <input type="radio" name="sfx" id="sfx-0" value="1" checked="checked">
                            1
                          </label>
                          <label class="radio-inline" for="sfx-1">
                            <input type="radio" name="sfx" id="sfx-1" value="2">
                            2
                          </label>
                          <label class="radio-inline" for="sfx-2">
                            <input type="radio" name="sfx" id="sfx-2" value="3">
                            3
                          </label>
                          <label class="radio-inline" for="sfx-3">
                            <input type="radio" name="sfx" id="sfx-3" value="4">
                            4
                          </label>
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="price">Price</label>
                        <div class="col-md-4">
                        <input id="price" name="price" type="text" placeholder="" class="form-control input-md">

                        </div>
                      </div>

                      <!-- Button -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4">
                          <button id="" name="" class="btn btn-primary">Save</button>
                        </div>
                      </div>

                      </fieldset>
                      </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
