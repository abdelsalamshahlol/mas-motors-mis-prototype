@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="panel-body">
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
            @endif
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">Transaction details</div>
                  <div class="panel-body">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-2">
                          Name
                        </div>
                        <div class="col-md10">
                            {{$data->name}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Phone
                        </div>
                        <div class="col-md10">
                            {{$data->phone}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Address
                        </div>
                        <div class="col-md10">
                            {{$data->address}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Email
                        </div>
                        <div class="col-md10">
                            {{$data->email}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Model
                        </div>
                        <div class="col-md10">
                            {{$data->model}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Year
                        </div>
                        <div class="col-md10">
                            {{$data->year}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          SFX
                        </div>
                        <div class="col-md10">
                            {{$data->sfx}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Color
                        </div>
                        <div class="col-md10">
                            {{$data->color}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          VIN #
                        </div>
                        <div class="col-md10">
                            {{$data->vin}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Price
                        </div>
                        <div class="col-md10">
                            {{$data->price}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2">
                          Purchase date
                        </div>
                        <div class="col-md10">
                            {{$data->created_at}}
                        </div>
                      </div>
                      <br>
                      <hr>
                      <a href="{{url()->previous()}}" class="btn btn-warning">Go back</a>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
