@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="panel-body">
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
            @endif
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">Sales record</div>
                  <div class="panel-body">
                    <table class="table table-bordered table-responsive table-condensed ">

                            <tr>
                              <td>No.</td>
                              <td>Name</td>
                              <td>Model</td>
                              <td>Year</td>
                              <td>Purchase Date</td>
                              <td>Actions</td>
                            </tr>

                        <tbody>
                          @foreach ($data as $sale)
                          <tr>
                            <td>{{(($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration }}</td>
                            <td>{{$sale->name}}</td>
                            <td>{{$sale->model}}</td>
                            <td>{{$sale->year}}</td>
                            <td>{{$sale->created_at}}</td>
                            <td>
                              <ul class="list-unstyled text-center">
                                <li><a href="{{route('sales.show',$sale->id)}}" class="btn btn-success" style="width: 68px;  margin-bottom: 4px;">View</a> </li>
                                <li><a href="{{route('sales.destroy',$sale->id)}}" class="btn btn-danger" style="width: 68px;  margin-bottom: 0px;">Delete</a></li>
                              </ul>
                            </td>
                          </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                      {{ $data->links() }}
                    </div>
                    <a href="{{route('home')}}" class="btn btn-warning">Go back</a>

                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
