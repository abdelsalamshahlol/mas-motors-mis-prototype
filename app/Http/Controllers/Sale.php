<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\carSale;
use Carbon\Carbon;
use DB;
use  Illuminate\Support\Collection;

class Sale extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data= carSale::paginate(20);
        return view('sales.record', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('sales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request)
        $sale=new carSale();
        $sale->name=$request->name;
        $sale->address=$request->address;
        $sale->phone=$request->Phone;
        $sale->email=$request->email;
        $sale->model=$request->model;
        $sale->color=$request->color;
        $sale->vin=$request->vin;
        $sale->sfx=$request->sfx;
        $sale->price=$request->price;
        $sale->year= Carbon::now()->year;
        $sale->save();
        return redirect()->route('home')->with('status', 'Add successfully');
    }

    public function reports()
    {
        $data= carSale::all();
        $c=count($data);
        $perModel = new Collection();
        $models = array('NG Corolla','Hilux DC 2.0','LC200 GXR' );
        foreach ($models as $model) {
            $perModel->push(DB::table('car_sales')->where('model', $model)->get());
        }
                          //_ _
        //AWESOME CODE HERE B_B
        $x=DB::table('car_sales')->select('price','created_at')->get()->groupBy(function($val) {
            return Carbon::parse($val->created_at)->format('Y');});
        // dd($x);
        $sales_income_per_year=array();
        foreach ($x as $year) {
          $sales_income_per_year[]=[Carbon::parse($year[0]->created_at)->format('Y'),$year->sum('price')];
        }
        return view('sales.reports', ['sales_count'=>$c,'models'=>$models,'perModel'=>$perModel,'sales_yearly'=>$sales_income_per_year]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=carSale::find($id);
        return view('sales.show', ['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
